from django.core.validators import FileExtensionValidator
from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField


class TourCategory(models.Model):
    title = models.CharField(
        verbose_name='Название',
        max_length=255
    )
    position = models.SmallIntegerField(
        verbose_name='Позиция',
        default=1
    )
    icon = models.FileField(
        verbose_name='Иконка',
        help_text='Загружать в форматах jpg/png или svg',
        validators=[
            FileExtensionValidator(['jpg', 'jpeg', 'png', 'svg'])
        ]
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Категория тура'
        verbose_name_plural = 'Категории туров'


class Tour(models.Model):
    face_image = models.ImageField(
        verbose_name='Главная фотография',
        upload_to='face_images/'
    )
    global_title = models.CharField(
        verbose_name='Главное название',
        max_length=255
    )
    mini_title = models.CharField(
        verbose_name='Второстепенное название/Мини описание',
        max_length=255,
        blank=True,
        null=True
    )
    created = models.DateTimeField(
        verbose_name='Создано',
        auto_now_add=True
    )
    amount = models.FloatField(
        verbose_name='Общая стоимость',
        default=0
    )
    is_active = models.BooleanField(
        verbose_name='Активен ?',
        default=True
    )
    is_hot = models.BooleanField(
        verbose_name='Популярен ?',
        default=False
    )
    background_image = models.ImageField(
        verbose_name='Фотография заднего вида',
        upload_to='background_image/',
        null=True,
        blank=True,
    )
    category = models.ForeignKey(
        TourCategory,
        verbose_name='Категория',
        on_delete=models.SET_NULL,
        null=True
    )

    def __str__(self):
        return self.global_title

    class Meta:
        verbose_name = 'Тур'
        verbose_name_plural = 'Туры'


class Day(models.Model):
    tour = models.ForeignKey(
        to=Tour,
        verbose_name='Тур',
        related_name='days',
        on_delete=models.CASCADE
    )
    number = models.SmallIntegerField(
        verbose_name='День',
        default=1,
    )
    content = RichTextUploadingField(
        verbose_name='Контент'
    )

    def __str__(self):
        return f"{self.tour.global_title} | Day {self.number}"

    class Meta:
        verbose_name = 'День'
        verbose_name_plural = 'Дни'


class TourImage(models.Model):
    tour = models.ForeignKey(
        to=Tour,
        verbose_name='Тур',
        related_name='images',
        on_delete=models.CASCADE
    )
    image = models.ImageField(
        verbose_name='Фотография',
        upload_to='tour_images/'
    )

    def __str__(self):
        return self.image.url

    class Meta:
        verbose_name = 'Фотография тура'
        verbose_name_plural = 'Фотографии туров'


class Slider(models.Model):
    image = models.ImageField(
        verbose_name='Картинка',
        upload_to='slider_image/'
    )
    title = models.CharField(
        verbose_name='Название',
        max_length=255
    )
    content = models.TextField(
        verbose_name='Контент',
        help_text='Отображается другим шрифтом поосторожней'
    )
    is_active = models.BooleanField(
        verbose_name='Активен ?',
        default=True
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Контент для слайдера'
        verbose_name_plural = 'Контент для слайдера'


class HomeContent(models.Model):
    title = models.CharField(
        verbose_name='Название',
        max_length=255
    )
    photo = models.ImageField(
        verbose_name='Фотография',
    )
    content = RichTextUploadingField(
        verbose_name='Контент'
    )
    link = models.CharField(
        verbose_name='Ссылка',
        max_length=255,
        null=True,
        blank=True
    )
    is_active = models.BooleanField(
        verbose_name='Активен ?',
        default=True
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Таблицы на главной странице'
        verbose_name_plural = 'Таблицы на главной странице'


class Order(models.Model):
    name = models.CharField(
        verbose_name='Имя',
        max_length=255
    )
    email = models.EmailField(
        verbose_name='Адресс электронной почты'
    )
    contact_data = models.CharField(
        verbose_name='Контактные данные',
        max_length=255
    )
    message = models.TextField(
        verbose_name='Сообщение от пользователя'
    )

    def __str__(self):
        return f"{self.name} | {self.email}"

    class Meta:
        verbose_name = 'Заказы'
        verbose_name_plural = 'Заказы'


class Sight(models.Model):
    image = models.ImageField(
        verbose_name='Фотография',
        upload_to='sights_images/'
    )

    def __str__(self):
        return self.image.url

    class Meta:
        verbose_name = 'Достопримечальность на главной странице(максимум 6 штук)'
        verbose_name_plural = 'Достопримечальности на главной странице(максимум 6 штук)'


class Hotel(models.Model):
    image = models.ImageField(
        verbose_name='Фотография',
        upload_to='hotels_image/'
    )

    def __str__(self):
        return self.image.url

    class Meta:
        verbose_name = 'Места проживания на главной ст' \
                       'ранице(максимум 6 штук)'
        verbose_name_plural = 'Места проживания на главной странице(максимум 6 штук)'


class WhyUs(models.Model):
    title = models.CharField(
        verbose_name='Заголовок',
        max_length=255
    )
    content = models.TextField(
        verbose_name='Контент'
    )
    logo = models.FileField(
        verbose_name='Логотип',
        upload_to='why_us_logo/'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Наши преимущества максимум(6 штук)'
        verbose_name_plural = 'Наши преимущества максимум(6 штук)'


class AboutUsFooter(models.Model):
    text = models.TextField(
        verbose_name='Текст'
    )

    class Meta:
        verbose_name = 'Текст для нижней части сайта'
        verbose_name_plural = 'Текст для нижней части сайта'


class NewsSection(models.Model):
    title = models.CharField(
        verbose_name='Заголовок',
        max_length=255
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Подраздел новостей'
        verbose_name_plural = 'Подраздел новостей'


class News(models.Model):
    title = models.CharField(
        verbose_name='Заголовок',
        max_length=255
    )
    image = models.ImageField(
        verbose_name='Фотгография',
        upload_to='news_image/'
    )
    content = models.TextField(
        verbose_name='Контент'
    )
    section = models.ForeignKey(
        NewsSection,
        verbose_name='Раздел',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'


class AboutUs(models.Model):
    text = models.TextField(
        verbose_name="Текст",
    )
    photo = models.ImageField(verbose_name='Фотография')

    class Meta:
        verbose_name = 'Текст на странице о нас'
        verbose_name_plural = 'Текст на странице о нас'


class AboutUs2(models.Model):
    text = models.TextField(
        verbose_name="Текст",
    )

    class Meta:
        verbose_name = 'Текст на странице о нас 2'
        verbose_name_plural = 'Текст на странице о нас 2'
