$(window).scroll(() => {
    if( $(window).scrollTop() > 125 ) {
        $('#header').css({position: 'fixed'});
        $('#header_placeholder').css({display: 'block'});
    } 
    else {
        $('#header').css({position: 'static', top: '-125px'});
        $('#header_placeholder').css({display: 'none'});
    }
});

$(document).ready(() => {
    const header = $("header .fusion-header");
    let headerHeight = 0;
    let shouldWrite = 1;
    $(window).on("scroll",function() {
        headerHeight = header.height();
        console.log(headerHeight);
        if (shouldWrite && headerHeight!==undefined) {
            $("#headerjumpfixer").html(".fusion-header-wrapper.fusion-is-sticky{padding-top:"+headerHeight + "px}");
            shouldWrite = 0;
        }
    });
$(window).on("resize",() => {
    shouldWrite = 1;
});
});