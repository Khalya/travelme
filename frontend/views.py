from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import TemplateView, ListView, DetailView

from core.helpers import send_email_message
from core.models import Tour, Slider, HomeContent, Order, TourCategory, Sight, Hotel, WhyUs, News, NewsSection, AboutUs, \
    AboutUs2


# Home View домашняя страница
class HomeView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['hot_tours'] = Tour.objects.filter(
            # Filtering objects
            is_active=True,
            is_hot=True
        ).prefetch_related('days').order_by(
            # Ordering
            '-id'
        ).only(
            # Only used fields
            'created',
            'global_title',
            'face_image'
        )
        context['categories'] = TourCategory.objects.order_by('-position')
        context['slider_objects'] = Slider.objects.filter(is_active=True)
        context['home_content'] = HomeContent.objects.filter(is_active=True)
        sights = Sight.objects.order_by('-id')[:6]
        context['sights_normal_form'] = sights[:3]
        context['sights_not_normal_form'] = sights[3:6]
        hotels = Hotel.objects.order_by('-id')[:6]
        context['hotels'] = hotels[:3]
        context['hotels_not_normal'] = hotels[3:6]
        context['why_us'] = WhyUs.objects.all()[:6]

        return context


class AboutUsView(TemplateView):
    template_name = 'abou-us.html'

    def get_context_data(self, **kwargs):
        context = super(AboutUsView, self).get_context_data(**kwargs)
        context['text'] = AboutUs.objects.first()
        context['text2'] = AboutUs2.objects.first()
        return context


class NewsView(ListView):
    template_name = 'news.html'
    model = News
    paginate_by = 3
    context_object_name = 'news'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['sections'] = NewsSection.objects.all()
        return context

    def get_queryset(self):
        queryset = super().get_queryset()
        section = self.request.GET.get('section', None)
        if section:
            return queryset.filter(section_id=section)
        else:
            return queryset.filter(section_id=NewsSection.objects.first().id)


class TourListView(ListView):
    template_name = 'tours.html'
    model = Tour
    context_object_name = 'tours'
    paginate_by = 3

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(TourListView, self).get_context_data(**kwargs)
        context['search'] = self.request.GET.get('global_title', None)
        return context

    def get_queryset(self):
        global_title = self.request.GET.get('global_title', None)
        category = self.request.GET.get('category', None)
        if category:
            queryset = super().get_queryset().filter(category_id=category)
            return queryset
        if global_title:
            queryset = super().get_queryset().filter(global_title__icontains=global_title)
            return queryset
        else:
            return super().get_queryset()



class TourDetailView(DetailView):
    template_name = 'detail.html'
    model = Tour
    context_object_name = 'tour'

    def post(self, request, pk):
        email = request.POST.get('email', None)
        description = request.POST.get('description', None)
        name = request.POST.get('name', None)

        if email and description and name:
            send_email_message(name, description, email)
        return redirect(reverse('tour-detail', kwargs={'pk': self.get_object().pk}))
