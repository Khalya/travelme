from django.contrib import admin
from core.models import Tour, Day, TourImage, Slider, HomeContent, Order, TourCategory, Sight, Hotel, WhyUs, \
    AboutUsFooter, NewsSection, News, AboutUs, AboutUs2
from tabbed_admin.admin import TabbedModelAdmin
from modeltranslation.admin import TranslationTabularInline, TranslationAdmin


class TourImageInline(admin.TabularInline):
    model = TourImage
    extra = 1


class DayInline(TranslationTabularInline):
    model = Day
    extra = 1


@admin.register(Tour)
class TourAdmin(TabbedModelAdmin, TranslationAdmin):
    list_display = ['global_title', 'amount', 'created', 'category', 'is_active']
    list_editable = ['is_active']

    tab_overview = (
        (None, {
            'fields': ['face_image', 'global_title', 'mini_title', 'category', 'amount', 'is_active', 'is_hot']
        }),

    )
    tab_days = (
        DayInline,
    )
    tab_tour_images = (
        TourImageInline,
    )
    tabs = [
        ('Тур', tab_overview),
        ('Дни', tab_days),
        ('Фотографии', tab_tour_images),
    ]


@admin.register(Slider)
class SliderAdmin(TranslationAdmin):
    pass


@admin.register(HomeContent)
class HomeContentAdmin(TranslationAdmin):
    pass


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    pass


@admin.register(TourCategory)
class TourCategoryAdmin(TranslationAdmin):
    pass


@admin.register(Sight)
class SightAdmin(admin.ModelAdmin):
    pass


@admin.register(Hotel)
class HotelAdmin(admin.ModelAdmin):
    pass


@admin.register(WhyUs)
class WhyUsAdmin(TranslationAdmin):
    pass


@admin.register(AboutUsFooter)
class AboutUsFooterAdmin(TranslationAdmin):
    pass


@admin.register(NewsSection)
class NewsSectionAdmin(TranslationAdmin):
    pass


@admin.register(News)
class NewsAdmin(TranslationAdmin):
    pass


@admin.register(AboutUs)
class AboutUsAdmin(TranslationAdmin):
    pass


@admin.register(AboutUs2)
class AboutUs2Admin(TranslationAdmin):
    pass
