from django import template

register = template.Library()


@register.filter(name='even_number')
def even_number(value, *args):
    return int(value) % 2 == 0


# Plusing objects in template
@register.simple_tag(name='plus')
def plus(a, b, *args, **kwargs):
    return a + b


# Minusing objects in template
@register.simple_tag(name='minus')
def minus(a, b, *args, **kwargs):
    return a - b




# Plusing objects in template
@register.simple_tag(name='plus')
def plus(a, b, *args, **kwargs):
    return a + b


# Minusing objects in template
@register.simple_tag(name='minus')
def minus(a, b, *args, **kwargs):
    return a - b


