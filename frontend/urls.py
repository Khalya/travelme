from django.urls import path
from .views import HomeView, AboutUsView, NewsView, TourDetailView, TourListView

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('about-us/', AboutUsView.as_view(), name='about-us'),
    path('news/', NewsView.as_view(), name='news'),
    path('tour/<int:pk>', TourDetailView.as_view(), name='tour-detail'),
    path('tours/', TourListView.as_view(), name='tour-list')
]
