'use strict'

const navWrapper = document.querySelector('.nav-wrapper')
const headerWrapper = document.querySelector('.header-wrapper')

window.addEventListener("scroll", () => {

    if (document.documentElement.scrollTop > 200) {
        document.body.style.paddingTop = navWrapper.offsetHeight + 'px'
        navWrapper.style.top = `-0px`
        document.body.classList.add('fixed')
    } else {
        document.body.style.paddingTop = 0 + 'px'
        navWrapper.style.top = `-${navWrapper.offsetHeight}px`
        document.body.classList.remove('fixed')
    }
})

const swiperHeader = new Swiper('.swiper-container', {
    direction: 'horizontal',
    loop: true,
    speed: 1200,
    grabCursor: true,

    autoplay: {
        delay: 5000,
    },

    breakpoints: {
        540: {
            slidesPerView: 1,
            slidesPerGroup: 1
        },
        600: {
            slidesPerView: 1,
            slidesPerGroup: 1
        },
        1000: {
            slidesPerView: 1,
            slidesPerGroup: 1
        },
    },
  
    on: {
        slideChangeTransitionStart: function () {
            const swiper = this;
            setTimeout(function () {
                const currentTitle = $(swiper.slides[swiper.activeIndex]).attr("data-title");
                const currentSubtitle = $(swiper.slides[swiper.activeIndex]).attr("data-subtitle");
            }, 300);
            gsap.to($(".current-title"), 0.2, {autoAlpha: 0, y: -40, ease: Power1.easeIn});
            gsap.to($(".current-subtitle"), 0.2, {autoAlpha: 0, y: -40, delay: 0.15, ease: Power1.easeIn});
        },
        slideChangeTransitionEnd: function () {
            const swiper = this;
            const currentTitle = $(swiper.slides[swiper.activeIndex]).attr("data-title");
            const currentSubtitle = $(swiper.slides[swiper.activeIndex]).attr("data-subtitle");
            $(".slide-captions").html(function() {
                return "<h2 class='current-title'>" + currentTitle + "</h2>" + "<h3 class='current-subtitle'>" + currentSubtitle + "</h3>";
            });
            gsap.from($(".current-title"), 0.2, {autoAlpha: 0, y: 40, ease: Power1.easeOut});
            gsap.from($(".current-subtitle"), 0.2, {autoAlpha: 0, y: 40, delay: 0.15, ease: Power1.easeOut});
        }
    }
});

const currentTitle = $(swiperHeader.slides[swiperHeader.activeIndex]).attr("data-title");
const currentSubtitle = $(swiperHeader.slides[swiperHeader.activeIndex]).attr("data-subtitle");

$(".slide-captions").html(() => {
  return "<h2 class='current-title'>" + currentTitle + "</h2>" + "<h3 class='current-subtitle'>" + currentSubtitle + "</h3>";
});

const swiperTour = new Swiper('.swiper-tour', {
    loop: true,
    grabCursor: true,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    breakpoints: {
        540: {
            slidesPerView: 1,
            slidesPerGroup: 1
        },
        600: {
            slidesPerView: 2,
            slidesPerGroup: 1
        },
        1000: {
            slidesPerView: 4,
            slidesPerGroup: 1
        },
    }
})

$('.owl-carousel').owlCarousel({
    loop: true,
    margin: 15,
    responsiveClass: true,
    responsive:{
        0:{
            items: 1,
            nav:false
        },
        600:{
            items: 2,
            nav:false
        },
        1000:{
            items: 3,
            nav:false,
            loop:false
        }
    }
})

const swiperModal = new Swiper('.modal-carousel', {
    loop: true,
    grabCursor: true,
    spaceBetween: 10,
    breakpoints: {
        640: {
            slidesPerView: 1,
        },
        800: {
            slidesPerView: 2,
        },
        1000: {
            slidesPerView: 3,
        },
        1140: {
            slidesPerView: 4,
        }
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
});

function openNav() {
    document.getElementById("filter").style.cssText = `
        width: 250px;
        box-shadow: 10px 10px 10px rgba(0,0,0,0.4);
    `;
}
  
function closeNav() {
    document.getElementById("filter").style.cssText = ``;
}