from modeltranslation.translator import register, TranslationOptions
from .models import Tour, Day, Slider, HomeContent, WhyUs, AboutUsFooter, NewsSection, News, TourCategory, \
    AboutUs, AboutUs2


@register(Tour)
class NewsTranslationOptions(TranslationOptions):
    fields = ('global_title', 'mini_title')


@register(TourCategory)
class TourCategoryOptions(TranslationOptions):
    fields = ('title',)


@register(Day)
class DayTranslationOptions(TranslationOptions):
    fields = ('content',)


@register(Slider)
class SliderTranslationOptions(TranslationOptions):
    fields = ('title', 'content')


@register(HomeContent)
class SliderTranslationOptions(TranslationOptions):
    fields = ('title', 'content')


@register(WhyUs)
class WhyUsOptions(TranslationOptions):
    fields = ('title', 'content')


@register(AboutUsFooter)
class WhyUsOptions(TranslationOptions):
    fields = ('text',)

@register(NewsSection)
class NewsSectionOptions(TranslationOptions):
    fields = ('title',)


@register(News)
class NewsOptions(TranslationOptions):
    fields = ('title', 'content')

@register(AboutUs)
class AboutUsOptions(TranslationOptions):
    fields = ('text',)

@register(AboutUs2)
class AboutUs2Options(TranslationOptions):
    fields = ('text',)
