from core.models import AboutUsFooter, TourCategory


def all_see(request):
    about_us_footer = AboutUsFooter.objects.first()
    category = TourCategory.objects.all()
    path_ru = request.path.replace('/en/', '/ru/')
    path_en = request.path.replace('/ru/', '/en/')

    return {
        'about_us_footer': about_us_footer,
        'category': category,
        'path_ru': path_ru,
        'path_en': path_en
    }
