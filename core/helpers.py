import requests


def send_email_message(title, description, to):
    r = requests.post(
        "https://api.mailgun.net/v3/mail.travelmeuz.com/messages",
        auth=("api", "35fe61e88d005273774bd996db27996c-30b9cd6d-a5449495"),
        data={"from": to,
              "to": ["infotravelmeuz@gmail.com"],
              "subject": title,
              "text": description
              })